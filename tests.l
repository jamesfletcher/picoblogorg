(load "org-http.l")
(load "routes.l")

(de skip (Title . Rest)
	(prinl Title " - SKIPPED"))

(de should (Title . Rest)
	(prin Title)
	(unless (eval (car Rest))
		(prin " - FAILLLLLLL")
		(prinl))
	(prinl))

(de http-post Route
 (pack (pipe
	(eval Route)
	(make (until (eof) (link (pack (line) "^J") ))))))

(de http-post-p Route
 (pack (pipe
	(eval (car Route))
	(make (until (eof) (link (pack (line) "^J") ))))))

(should "get list of users"
	(let Users (org-to-users (read-org))
		(> (length Users) 0)))

(should "authenticate"
	(let (*Username "test"
				*Password "test"
				org-to-users '((X) '((Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL))))
		(_authenticate *Username *Password)))

(should "decrypt a session"
	(let (User '(Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)	
				Session (encrypt-string (str User))
				Decrypted (str (decrypt-string Session)))
		(= User Decrypted)))

(should "redirect to list if successful login"
	(let (*Post "Y" 
				*Username "test"
				*Password "test"
				org-to-users '((X) '((Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)))
				Html (chop (http-post login)))
		(match '(@ ~(chop "Location: !article-list") @) Html)))

(should "show a message on bad login"
	(let (*Post "Y" 
				*Username "test"
				*Password "NOPE!"
				org-to-users '((X) '((Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)))
				Html (chop (http-post login)))
		(match '(@ ~(chop "Invalid username or password") @) Html)))


(de create-test-file ()
	(let (Org (org-parse-text "* Blog^J** Post1^JCLOSED: 2012-01-01 Fri 01:00^Jtest^J* Private^J** Private1^JCLOSED: 2012-01-01 Fri 02:00^Jprivate")
				Text (org-to-text Org))
	 (out File (prin Text))
	 Org))


(should "render a list"
	(let (File "test.org" 
				Org (create-test-file)
				Html (chop (http-post article-list)))
		(match '(@ ~(chop "Post1") @) Html)))


(should "show post form"
	(let (File "test.org" 
				User '(Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)	
				Session (encrypt-string (str User))
				org-to-users '((X) '((Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)))
				*Cookies (list (cons 'Session Session))
				Org (create-test-file)
				Html (chop (http-post article-new)))
		(match '(@ ~(chop "textarea") @) Html)))


(should "post record"
	(let (File "test.org" 
				User '(Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)	
				Session (encrypt-string (str User))
				org-to-users '((X) '((Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)))
				*Cookies (list (cons 'Session Session))
				Org (create-test-file)
				*Post "Y" 
				*Text "hello world"
				*Title "Post2"
				commit-blog '((X))
				PostHtml (chop (http-post article-create))
				NewID (match '(@ ~(chop "!article-edit?") @ID "^J" @M) PostHtml)
				Html (chop (http-post article-list)))
			NewID))


(should "show edit form"
	(let (File "test.org" 
				User '(Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)	
				Session (encrypt-string (str User))
				org-to-users '((X) '((Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)))
				*Cookies (list (cons 'Session Session))
				Org (create-test-file)
				Html (chop (http-post-p (article-edit 1))))
		(match '(@ ~(chop "textarea") @) Html)))


(should "update a record"
	(let (File "test.org" 
				User '(Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)	
				Session (encrypt-string (str User))
				org-to-users '((X) '((Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)))
				*Cookies (list (cons 'Session Session))
				Org (create-test-file)
				*Post "Y" 
				*Text "hello world"
				*Title "Post2"
				commit-blog '((X))
				PostHtml (chop (http-post-p (article-update "1")))
				NewID (match '(@ ~(chop "!article-edit?") @ID "^J" @M) PostHtml)
				Html (chop (http-post article-list)))
			NewID))

(should "delete a record"
	(let (File "test.org" 
				User '(Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)	
				Session (encrypt-string (str User))
				org-to-users '((X) '((Username "test" Password "28275a6d4cea51f96b9507893d5f0ad5" Group NIL)))
				*Cookies (list (cons 'Session Session))
				Org (create-test-file)
				*Post "Y" 
				*Text "hello world"
				*Title "Post2"
				*Visibility "Blog"
				commit-blog '((X))
				PostHtml (chop (http-post article-create))
				NewID (match '(@ ~(chop "!article-edit?") @ID "^J" @M) PostHtml)
				Html (chop (http-post article-list))
				Saved (match '(@ ~(chop "Post2") @) Html)
				*ID (car @ID)
				DeleteResult (http-post article-delete)
				Html (chop (http-post article-list))
				Deleted (not (match '(@ ~(chop "Post2") @) Html)))
			(and Saved Deleted)))
